/*
 * qapass_lcd.c
 *
 *  Created on: 2. lis 2018.
 *      Author: Toni Varga
 */

#include "qapass_lcd.h"
#include "driver/i2c.h"
#include "freertos/FreeRTOS.h"
#include <esp_log.h>
#include "esp_types.h"

/**
 * @brief Initialization of the i2c bus for QAPASS Display
 *
 * @param void
 *
 * @return
 *     - ESP_OK Success
 *     - ESP_ERR_INVALID_ARG Parameter error
 *     - ESP_FAIL Driver install error
 */
void i2c_bus_init(void) {
	esp_err_t ret = ESP_OK;

    i2c_config_t conf;
    conf.mode = I2C_MODE_MASTER;
    conf.sda_io_num = QAPASS_SDA_PIN;
    conf.sda_pullup_en = GPIO_PULLUP_ENABLE;
    conf.scl_io_num = QAPASS_SCL_PIN;
    conf.scl_pullup_en = GPIO_PULLUP_ENABLE;
    conf.master.clk_speed = QAPASS_I2C_FREQ_HZ;

    ret = i2c_param_config(QAPASS_I2C_PORT, &conf);
    if(ret != ESP_OK) {
    	ESP_LOGE(QAPASS_LOG_TAG, "i2c_param_config failed: %s", esp_err_to_name(ret));
    }

    ret = i2c_driver_install(QAPASS_I2C_PORT, conf.mode, 0, 0, 0);
    if(ret != ESP_OK) {
    	ESP_LOGE(QAPASS_LOG_TAG, "i2c_driver_install failed: %s", esp_err_to_name(ret));;
    }
    vTaskDelay(50 / portTICK_PERIOD_MS);
}

/**
 * @brief Sends 4 bits to the display
 *
 * @param byte: value that is set on the pins
 * @param rw: value to be set on the rw pin (0 or 1)
 * @param data: value to be set on the rs pin (0 or 1)
 *
 * @return void
 */
void send4bits(uint8_t byte, uint8_t rw, uint8_t rs) {
	uint8_t bits = (byte << 4) | (rw << RW) | (rs<<RS) | (1<<BACKLIGHT);
	ESP_LOGI(QAPASS_LOG_TAG, "bits: %x", bits);

	i2c_cmd_handle_t cmd = i2c_cmd_link_create();
    i2c_master_start(cmd);

    //Handling for initial set of rw, rs
    i2c_master_write_byte(cmd, (QAPASS_ADDR << 1) | I2C_MASTER_WRITE, 1);
    i2c_master_write_byte(cmd, (rw << RW) | (rs<<RS) | (1<<BACKLIGHT), 1); //Set rs and rw to desired values and turn backlight on
    vTaskDelay(1 / portTICK_PERIOD_MS);

    //Sending data
    i2c_master_write_byte(cmd, bits, 1); //Send 4 bits keeping rs and rw as initialized
    vTaskDelay(1 / portTICK_PERIOD_MS);

    //Toggle enable bit
    i2c_master_write_byte(cmd, bits | (1<<E), 1);
    vTaskDelay(1 / portTICK_PERIOD_MS);
    i2c_master_write_byte(cmd, bits | (0<<E), 1);

    i2c_master_stop(cmd);
    esp_err_t ret = i2c_master_cmd_begin(QAPASS_I2C_PORT, cmd, 1000 / portTICK_RATE_MS);
    if(ret != ESP_OK) ESP_LOGE(QAPASS_LOG_TAG, "i2c_master_cmd_begin failed: %s", esp_err_to_name(ret));
    i2c_cmd_link_delete(cmd);
}

/**
 * @brief Initialization of QAPASS LCD Display
 * 		@note
 * 		This will work only for 2x16 QAPASS LCD that is driven with PCF8574T
 * 		I2C expander that has it's pins connected as drawn in the schematic
 *
 * @param void
 *
 * @return void
 */
void lcd_init(void) {
	i2c_bus_init();
	send4bits(0x03, 0, 0);
	vTaskDelay(5 / portTICK_PERIOD_MS);
	send4bits(0x03, 0, 0);
	vTaskDelay(5 / portTICK_PERIOD_MS);
	send4bits(0x03, 0, 0);
	//Now we should be in 4 bit interface

	//Function set (Set interface to be 4 bits long.)
	send4bits(0x02, 0, 0);
	//Function set
	send4bits(0x02, 0, 0);
	send4bits(0x08, 0, 0);

	//Display off
	send4bits(0x00, 0, 0);
	send4bits(0x08, 0, 0);

	//Display clear
	send4bits(0x00, 0, 0);
	send4bits(0x01, 0, 0);

	//Entry mode set
	send4bits(0x00, 0, 0);
	send4bits(0x06, 0, 0);

	//Display on, cursor on, no cursor blink
	send4bits(0x00, 0, 0);
	send4bits(0x0E, 0, 0);

	//TODO: Create some custom characters (up and down arrows)
}

/**
 * @brief Sets the cursor to desired location on the display
 *
 * @param x: x-coordinate on the display (0-15)
 * @param y: y-coordinate on the display (0-1)
 *
 * @return void
 */
void lcd_set_cursor(uint8_t x, uint8_t y) {
	uint8_t address = 0;
	uint8_t haddr;
	uint8_t laddr;

	//First row starts with address 0, second row starts with addres 0x40
	(y > 0) ? (address = 0x40 + x) : (address = x);
	address |= (1 << 7);

	laddr = address;
	haddr = address >> 4;

	ESP_LOGI(QAPASS_LOG_TAG, "Address: %x", address);

	send4bits(haddr, 0, 0);
	send4bits(laddr, 0, 0);

	vTaskDelay(1 / portTICK_PERIOD_MS);
}

/**
 * @brief Writes the string to the last set cursor location
 *
 * @param string: a pointer to a string literal to be written
 * 					(must include null termination character)
 *
 * @return void
 */
void lcd_write_string(char* string) {
	uint8_t hbyte;
	uint8_t lbyte;

	ESP_LOGI(QAPASS_LOG_TAG, "string: %s", string);

	while(*string != '\0') {
		ESP_LOGI(QAPASS_LOG_TAG, "letter: %c", *string);

		lbyte = *string++;
		hbyte = lbyte >> 4;

		ESP_LOGI(QAPASS_LOG_TAG, "string high: %x", hbyte);
		ESP_LOGI(QAPASS_LOG_TAG, "string low: %x", lbyte);

		send4bits(hbyte, 0, 1);
		send4bits(lbyte, 0, 1);
	}
}

/**
 * @brief Clears the display and sets the cursor to the DDRAM address 0
 * 		@note
 * 		does not returns the diyplay to original position if it was shifted
 *
 * @param void
 *
 * @return void
 */
void lcd_clear(void) {
	send4bits(0x00, 0, 0);
	send4bits(0x01, 0, 0);
	vTaskDelay(2 / portTICK_PERIOD_MS);
}

/**
 * @brief Clears the display, sets the cursor to the DDRAM address 0 and
 * 			returns the display to original position if it was shifted
 *
 * @param void
 *
 * @return void
 */
void lcd_home(void) {
	send4bits(0x00, 0, 0);
	send4bits(0x02, 0, 0);
	vTaskDelay(2 / portTICK_PERIOD_MS);
}
