/*
 * qapass_lcd.h
 *
 *  Created on: 2. lis 2018.
 *      Author: Toni Varga
 */
#include <inttypes.h>

#ifndef __QAPASS_LCD__
#define __QAPASS_LCD__

#define HOME_CLEAR_ACKED

#ifndef HOME_CLEAR_ACKED
	#warning "Newer use lcd_clear() and lcd_home() consecutively!!!"
#endif

#define QAPASS_LOG_TAG							"QAPASS"

#define QAPASS_ADDR								0x27
#define QAPASS_SCL_PIN							GPIO_NUM_25
#define QAPASS_SDA_PIN							GPIO_NUM_26
#define QAPASS_I2C_PORT							I2C_NUM_1
#define QAPASS_I2C_FREQ_HZ         				100000

#define RS										0
#define RW										1
#define E										2
#define BACKLIGHT								3

void lcd_init(void);
void lcd_set_cursor(uint8_t x, uint8_t y);
void lcd_write_string(char* string);
void lcd_clear(void);
void lcd_home(void);

#endif
