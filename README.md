This is a library for QAPASS 1602A 2x16 LCD Display, driven over PCF8574T I2C expander.
It is going to be added as submodule or a component to other ESP-IDF projects.
The library works only if the I2C expander is wired as shown in the schematic. (The schematics will be added soon)

This readme will be updated as the project advances.